package org.geekhub.volodymyr;

import java.util.Scanner;

class Hw3Main {
        public static void main(String[] args) {
        System.out.println("Please enter words quantity (1-100)  :");
        Scanner input = new Scanner(System.in);
        byte wordsQuantity;
        wordsQuantity = input.nextByte();
        input.nextLine();
        String[] words = new String[wordsQuantity];
        final byte ABBREVIATION_LENGTH = 10;
        for (int i = 0; i < wordsQuantity; i++) {
            System.out.print("Enter [" + (i + 1) + "] word:");
            words[i] = input.nextLine();
            if (words[i].length() > ABBREVIATION_LENGTH) {
                words[i] = words[i].substring(0, 1) + (words[i].length() - 2) + words[i].substring((words[i].length() - 1));
            }
        }
        for (int i = 0; i < wordsQuantity; i++) {
            System.out.println(words[i]);
        }
    }
}
