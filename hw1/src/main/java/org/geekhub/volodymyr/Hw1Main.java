package org.geekhub.volodymyr;

import java.util.Scanner;

class Hw1Main {
    private static String calculateSumm(String phone) {
        int summ = 0;
        for (int i = 0; i < phone.length(); i++) {
            summ = summ + Integer.parseInt(String.valueOf(phone.charAt(i)));
        }
        return Integer.toString(summ);
    }

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        boolean PhoneValid;
        String phoneNumber;
        do {
            PhoneValid = true;
            System.out.println("Please enter phone number:");
            phoneNumber = input.nextLine();
            if (phoneNumber.length() < 10 | 14 < phoneNumber.length()) {
                PhoneValid = false;
            }
            char phoneSymbol;
            for (int i = 0; i < phoneNumber.length(); i++) {
                phoneSymbol = (phoneNumber.charAt(i));
                if ((phoneSymbol == '0')) {
                } else if (phoneSymbol == '1') {
                } else if (phoneSymbol == '2') {
                } else if (phoneSymbol == '3') {
                } else if (phoneSymbol == '4') {
                } else if (phoneSymbol == '5') {
                } else if (phoneSymbol == '6') {
                } else if (phoneSymbol == '7') {
                } else if (phoneSymbol == '8') {
                } else if (phoneSymbol == '9') {
                } else if (i == 0 && phoneSymbol == '+') {
                } else PhoneValid = false;
            }
            String operatorCode;
            if (phoneNumber.length() > 10) {
                operatorCode = phoneNumber.substring(phoneNumber.length() - 10, phoneNumber.length() - 7);
            } else operatorCode = phoneNumber.substring(0, 3);
            switch (operatorCode) {
                case "067":
                case "098":
                case "097":
                case "063":
                case "073":
                case "093":
                case "095":
                case "099":
                    break;
                default:
                    PhoneValid = false;
                    break;
            }
            if (!PhoneValid) {
                System.out.println("Phone number is incorrect");
            }
        } while (!PhoneValid);
        if (phoneNumber.charAt(0) == '+') {
            phoneNumber = phoneNumber.substring(1);
        }
        String summa = phoneNumber;
        int calcCounter = 1;
        do {
            summa = calculateSumm(summa);
            System.out.println(calcCounter + "st round of calculation " + summa);
            calcCounter++;
        }
        while (summa.length() != 1);
        System.out.print("Final result is: ");
        switch (summa) {
            case "1":
                System.out.println("One");
                break;
            case "2":
                System.out.println("Two");
                break;
            case "3":
                System.out.println("Three");
                break;
            case "4":
                System.out.println("Four");
                break;
            default:
                System.out.print(summa);
        }
    }
}
