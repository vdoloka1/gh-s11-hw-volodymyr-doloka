<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c' %>
<html>
<head>
    <title>GuestBook</title>
</head>
<body>
<h2>Guest book</h2>
<hr>
<h3>Leave feedback</h3>

<form action="/" method="post">
    <label for="name">Name:</label>
    <input type="text" id="name" name="name" size="23">
    <br>
    <label for="msg">Message:</label>
    <textarea id="msg" name="message"></textarea>
    <br>
    <label for="rating">Rating:</label>
    <select id="rating" name="rating">
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5" selected>5</option>
    </select>
    <br>
    <button type="submit">Send</button>
</form>

<h3>Feedbacks</h3>
<c:forEach items="${entries}" var="entrie">
<p> ${entrie.date}  name= ${entrie.name} , message= ${entrie.message},rating = ${entrie.rating}  </p>
</c:forEach>

</body>
</html>
