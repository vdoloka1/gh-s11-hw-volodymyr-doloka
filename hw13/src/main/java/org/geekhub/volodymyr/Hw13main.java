package org.geekhub.volodymyr;

import org.geekhub.volodymyr.repository.GuestBookEntry;
import org.geekhub.volodymyr.repository.GuestBookRepository;
import org.geekhub.volodymyr.repository.GuestBookRepositoryImpl;

import java.io.*;
import java.time.LocalDateTime;
import java.util.*;
import javax.servlet.*;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;

@WebServlet("/")
public class Hw13main extends HttpServlet {
   private GuestBookRepository guestBookRepository;

    @Override
    public void init() {
        guestBookRepository = new GuestBookRepositoryImpl();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        List<GuestBookEntry> entries = guestBookRepository.getEntries();
        Collections.reverse(entries);
        request.setAttribute("entries", entries);
        request.getRequestDispatcher("/WEB-INF/index.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setContentType("text/html");
        String name = request.getParameter("name");
        String message = request.getParameter("message");
        String rating = request.getParameter("rating");
        int mark = Integer.parseInt(rating);
        LocalDateTime date = LocalDateTime.now();
        GuestBookEntry guestBookEntry = new GuestBookEntry(name, message, mark, date);
        guestBookRepository.addEntry(guestBookEntry);
        response.sendRedirect("/");
    }
}