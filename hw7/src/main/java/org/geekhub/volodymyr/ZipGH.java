package org.geekhub.volodymyr;

import java.io.*;
import java.util.Objects;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class ZipGH {
    private final File InitialDirectory;
    private final String zipFile;
    private final FileFilter fileFilter;
    private ZipOutputStream zipOutputStream;

    private void zipDirectoryFiles(File directory, FileFilter fileFilter) {
        File[] srcFiles = directory.listFiles(fileFilter);
        try {
            byte[] buffer = new byte[1024];
            for (File srcFile : Objects.requireNonNull(srcFiles)) {
                InputStream in = new BufferedInputStream(new FileInputStream(srcFile));
                zipOutputStream.putNextEntry(new ZipEntry(srcFile.getName()));
                int length;
                while ((length = in.read(buffer)) > 0) {
                    zipOutputStream.write(buffer, 0, length);
                }
                zipOutputStream.closeEntry();
                in.close();
            }
        } catch (IOException ex) {
            System.out.println("Exeption: " + ex);
        }
        FileFilter DirectoryFilter = File::isDirectory;
        File[] NestedDirectories = directory.listFiles(DirectoryFilter);
        for (File CurrentDirectory : Objects.requireNonNull(NestedDirectories)) {
            zipDirectoryFiles(CurrentDirectory, fileFilter);
        }
    }

    public void zipFiles() throws IOException {
        OutputStream out = new BufferedOutputStream(new FileOutputStream(zipFile));
        zipOutputStream = new ZipOutputStream(out);
        zipDirectoryFiles(InitialDirectory, fileFilter);
        out.close();
        zipOutputStream.flush();
        zipOutputStream.close();
    }

    public ZipGH(File source, String zipFile, FileFilter fileFilter) {
        this.InitialDirectory = source;
        this.zipFile = zipFile;
        this.fileFilter = fileFilter;
    }
}
