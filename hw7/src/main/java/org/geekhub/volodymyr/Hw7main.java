package org.geekhub.volodymyr;

import java.io.*;

public class Hw7main {
    public static void main(String[] args) {
        try {
            File source = new File("test files");
            String zipFile = "audio.zip";
            FileFilter audioFilter = file -> file.getName().endsWith(".mp3") | file.getName().endsWith(".wav") |
                    file.getName().endsWith(".wma");
            ZipGH audioZip = new ZipGH(source, zipFile, audioFilter);
            audioZip.zipFiles();
            zipFile = "video.zip";
            FileFilter videoFilter = file -> file.getName().endsWith(".avi") | file.getName().endsWith(".mp4") |
                    file.getName().endsWith(".flv");
            ZipGH videoZip = new ZipGH(source, zipFile, videoFilter);
            videoZip.zipFiles();
            zipFile = "image.zip";
            FileFilter imageFilter = file -> file.getName().endsWith("jpeg") | file.getName().endsWith("jpg") |
                    file.getName().endsWith("gif") | file.getName().endsWith("png");
            ZipGH imageZip = new ZipGH(source, zipFile, imageFilter);
            imageZip.zipFiles();
        } catch (Exception ex) {
            System.out.println("Exeption: " + ex);
        }
    }
}