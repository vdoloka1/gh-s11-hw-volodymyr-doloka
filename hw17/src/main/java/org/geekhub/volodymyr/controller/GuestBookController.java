package org.geekhub.volodymyr.controller;

import org.geekhub.volodymyr.repository.GuestBookEntry;
import org.geekhub.volodymyr.repository.GuestBookRepositoryImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.time.LocalDateTime;
import java.util.List;

@Controller
public class GuestBookController {
    final private GuestBookRepositoryImpl guestBookRepository;

    @Autowired
    public GuestBookController(GuestBookRepositoryImpl guestBookRepositorySQL) {
        this.guestBookRepository = guestBookRepositorySQL;
    }

    @GetMapping("/guestbook")
    public String getMainPage() {
        return "index";
    }

    @GetMapping("/guestbook/Ajax")
    @ResponseBody
    public List<GuestBookEntry> getEntries(@RequestParam(name = "page", required = false, defaultValue = "1") int page,
                                            @RequestParam(name = "itemPerPage", required = false, defaultValue = "10") int itemPerPage) {
        return guestBookRepository.getEntries(page, itemPerPage);
    }

    @PostMapping("/guestbook")
    public void formPost(@RequestParam(name = "name", required = false, defaultValue = "Unknown") String name,
                         @RequestParam(name = "message", required = false, defaultValue = "") String message,
                         @RequestParam(name = "rating", required = false, defaultValue = "5") String rating) {
        int mark = Integer.parseInt(rating);
        LocalDateTime date = LocalDateTime.now();
        GuestBookEntry guestBookEntry = new GuestBookEntry(name, message, mark, date);
        guestBookRepository.addEntry(guestBookEntry);
    }
}