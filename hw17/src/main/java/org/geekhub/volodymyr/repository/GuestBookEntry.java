package org.geekhub.volodymyr.repository;

import java.time.LocalDateTime;
import java.util.Objects;

public class GuestBookEntry implements Comparable<GuestBookEntry> {
    private final String name;
    private final String message;
    private final int rating;
    private final LocalDateTime date;

    public GuestBookEntry(String name, String message, int rating, LocalDateTime date) {
        this.name = name;
        this.message = message;
        this.rating = rating;
        this.date = date;
    }

    public String getName() {
        return name;
    }

    public String getMessage() {
        return message;
    }

    public int getRating() {
        return rating;
    }

    public LocalDateTime getDate() {
        return date;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof GuestBookEntry)) return false;
        GuestBookEntry entry = (GuestBookEntry) o;

        return rating == entry.rating && Objects.equals(name, entry.name) && Objects.equals(message, entry.message) && date.equals(entry.date);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, message, rating, date);
    }

    @Override
    public int compareTo(GuestBookEntry o) {

        return date.compareTo(o.date);
    }
}
