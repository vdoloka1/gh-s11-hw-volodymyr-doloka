package org.geekhub.volodymyr.repository;

import java.util.List;

public interface GuestBookRepository {

    void addEntry(GuestBookEntry guestBookEntry);

    List<GuestBookEntry> getEntries(int Page, int itemPerPage);

    int getEntriesCount();
}
