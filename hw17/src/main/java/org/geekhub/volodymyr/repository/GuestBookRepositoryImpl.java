package org.geekhub.volodymyr.repository;

import org.geekhub.volodymyr.repository.mapper.GuestBookRowMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
public class GuestBookRepositoryImpl implements GuestBookRepository {

    private final NamedParameterJdbcTemplate namedjdbcTemplate;
    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public GuestBookRepositoryImpl(NamedParameterJdbcTemplate namedjdbcTemplate, JdbcTemplate jdbcTemplate) {
        this.namedjdbcTemplate = namedjdbcTemplate;
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public void addEntry(GuestBookEntry guestBookEntry) {
        String sql = "INSERT INTO guestbook (name, message,rating,date ) VALUES (:name,:message,:rating,:date)";
        namedjdbcTemplate.update(sql,
                Map.of("name", guestBookEntry.getName(), "message", guestBookEntry.getMessage(),
                        "rating", guestBookEntry.getRating(), "date", guestBookEntry.getDate()));
    }

    @Override
    public List<GuestBookEntry> getEntries(int page, int itemPerPage) {
        String sql = "SELECT name, message,rating,date  FROM guestbook ORDER BY date DESC LIMIT " + itemPerPage
                + " OFFSET " + (page - 1) * itemPerPage;


        return namedjdbcTemplate.query(sql, new GuestBookRowMapper());
    }

    @Override
    public int getEntriesCount() {
        String sql = "SELECT COUNT(*) FROM guestbook";

        return jdbcTemplate.queryForObject(sql, Integer.class);
    }

}
