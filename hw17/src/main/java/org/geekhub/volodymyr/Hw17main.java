package org.geekhub.volodymyr;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Hw17main {
    public static void main(String[] args) {
        SpringApplication.run(Hw17main.class, args);
    }
}
