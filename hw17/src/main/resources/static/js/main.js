let page = 1;

function showFeedbacks() {
    $.ajax({
        url: 'guestbook/Ajax?page=' + page,
        dataType: "json",
        success: function (data) {
            let feedbacks = '';
            $.each(data, function (i, feedback) {
                feedbacks += "" + feedback.name + "" + feedback.message + feedback.rating + feedback.date + "<br>";
            });
            console.log(page++)
            $('#feedbacks').append(feedbacks);
        }
    });
}

function addFeedback(data) {
    $.ajax({
        url: 'guestbook?page=' + page,
        type: "POST",
        data: data,
        success: function () {
            console.log(data);
        }
    });
}

$(() => {
    $('form').on('submit', function (event) {
        event.preventDefault();
        const data = $('form').serialize();
        addFeedback(data);
        let feedback = $('#name').val() + "" + $('#message').val() + $('#rating').val() + new Date() + "<br>";
        $('#feedbacks').prepend(feedback);
        $('#form')[0].reset();
    });
});

$(function () {
    showFeedbacks();
});