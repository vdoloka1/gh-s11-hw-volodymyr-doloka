package org.geekhub.volodymyr.task2;

import java.util.Objects;

class Mentor {
    private final String fullName;

    public Mentor(String fullName) {
        this.fullName = fullName;
    }

    @Override
    public String toString() {
        return "'" + fullName + '\'';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Mentor)) return false;
        Mentor mentor = (Mentor) o;
        return fullName.equals(mentor.fullName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(fullName);
    }
}

