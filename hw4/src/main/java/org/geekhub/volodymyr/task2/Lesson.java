package org.geekhub.volodymyr.task2;

public class Lesson {
    private int number;
    private String LectionTheme;
    private boolean isStudentAbsentOnLection;
    private double HomeWorkMark;
    private boolean isHomeWorkIntimeFinish;
    private Mentor Reviewer;

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public String getLectionTheme() {
        return LectionTheme;
    }

    public void setLectionTheme(String lectionTheme) {
        LectionTheme = lectionTheme;
    }

    public boolean studentAbsentOnLection() {
        return isStudentAbsentOnLection;
    }

    public void setStudentAbsentOnLection(boolean studentAbsentOnLection) {
        isStudentAbsentOnLection = studentAbsentOnLection;
    }

    public double getHomeWorkMark() {
        return HomeWorkMark;
    }

    public void setHomeWorkMark(double homeWorkMark) {
        HomeWorkMark = homeWorkMark;
    }

    public boolean isHomeWorkIntimeFinish() {
        return isHomeWorkIntimeFinish;
    }

    public void setHomeWorkIntimeFinish(boolean homeWorkIntimeFinish) {
        isHomeWorkIntimeFinish = homeWorkIntimeFinish;
    }

    public Mentor getReviewer() {
        return Reviewer;
    }

    public void setReviewer(Mentor reviewer) {
        this.Reviewer = reviewer;
    }

}