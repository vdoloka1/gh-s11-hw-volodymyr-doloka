package org.geekhub.volodymyr.task2;

import java.util.*;

public class Course {
    final private List<Mentor> mentors = new ArrayList<>();
    final private List<Student> students = new ArrayList<>();
    final private Map<Student, List<Lesson>> lessons = new HashMap<>();

    public List<Mentor> getMentors() {
        return mentors;
    }

    public List<Student> getStudents() {
        return students;
    }

    public void doLesson(int lectionNumber, String LectionTheme) {
        for (Student student : students) {
            Lesson lesson = new Lesson();
            lesson.setNumber(lectionNumber);
            lesson.setLectionTheme(LectionTheme);
            boolean isPresentOnLection;
            isPresentOnLection = new Random().nextBoolean();
            lesson.setStudentAbsentOnLection(isPresentOnLection);
            List<Lesson> studentLessons = new ArrayList<>();
            if (lessons.containsKey(student)) {
                studentLessons = lessons.get(student);
            }
            studentLessons.add(lesson);
            lessons.put(student, studentLessons);
        }
    }

    public void checkHomework(Mentor mentor, Student student, int lessonNumber) {
        Lesson lesson;
        lesson = lessons.get(student).get(lessonNumber - 1);
        double randomMark = new Random().nextInt(9) * 0.5 + 1;
        lesson.setHomeWorkMark(randomMark);
        boolean HomeWorkIntimeFinish;
        HomeWorkIntimeFinish = new Random().nextBoolean();
        lesson.setHomeWorkIntimeFinish(HomeWorkIntimeFinish);
        lesson.setReviewer(mentor);
    }

    public void deletePassiveStudents() {
        List<Student> studentsToDelete = new ArrayList<>();
        final double CRITICAL_MARK = 3.5;
        final int CRITICAL_OUTDATED_HOMEWORKS = 3;
        for (Student student : lessons.keySet()) {
            Double averageMark = 0.0;
            Double outdatedHomeworksNumber = 0.0;
            for (Lesson lesson : lessons.get(student)) {
                averageMark += lesson.getHomeWorkMark();
                if (!lesson.isHomeWorkIntimeFinish()) {
                    outdatedHomeworksNumber++;
                }
            }
            if (averageMark < CRITICAL_MARK) {
                studentsToDelete.add(student);
            } else {
                if (outdatedHomeworksNumber > CRITICAL_OUTDATED_HOMEWORKS) {
                    studentsToDelete.add(student);
                }
            }
        }
        students.removeAll(studentsToDelete);
        System.out.println("Delete " + studentsToDelete.size() + " passive students:");
        System.out.println(studentsToDelete);
    }

    public void PrintStudentMaxSkippedLections() {
        Student studentTruant = new Student();
        int maxSkippedLectionsCounter = 0;
        for (Student student : lessons.keySet()) {
            int studentSkipedLectionCounter = 0;
            for (Lesson lesson : lessons.get(student)) {
                if (lesson.studentAbsentOnLection()) {
                    studentSkipedLectionCounter++;
                }
                if (studentSkipedLectionCounter > maxSkippedLectionsCounter) {
                    maxSkippedLectionsCounter = studentSkipedLectionCounter;
                    studentTruant = student;
                }
            }
        }
        System.out.println("Student truant " + studentTruant);
        System.out.println("skipped lections:");
        for (Lesson lesson : lessons.get(studentTruant)) {
            if (lesson.studentAbsentOnLection()) {
                System.out.println(lesson.getNumber() + ". " + lesson.getLectionTheme());
            }
        }
    }

    public void PrintMentorMaxMark5set() {
        final double MARK_VALUE_TO_COUNT = 5;
        int maxMarkCounter = 0;
        Mentor mentorMaxMark5set = new Mentor("No mentor set mark 5");
        for (Mentor mentor : getMentors()) {
            int currentMentorMarkCounter
                    = 0;
            for (Student student : lessons.keySet()) {
                for (Lesson lesson : lessons.get(student)) {
                    if (lesson.getHomeWorkMark() == MARK_VALUE_TO_COUNT & lesson.getReviewer().equals(mentor)) {
                        currentMentorMarkCounter++;
                    }
                }
                if (currentMentorMarkCounter > maxMarkCounter) {
                    maxMarkCounter = currentMentorMarkCounter;
                    mentorMaxMark5set = mentor;
                }
            }
        }
        System.out.println("Mentor setted max Mark " + MARK_VALUE_TO_COUNT + " :" + mentorMaxMark5set);
        System.out.println(maxMarkCounter + " times");
    }
}