package org.geekhub.volodymyr.task2;

public class Student {
    private final String fullName;

    public Student(String fullName) {
        this.fullName = fullName;
    }

    public Student() {
        fullName = null;
    }

    @Override
    public String toString() {
        return "'" + fullName + '\'';
    }
}




