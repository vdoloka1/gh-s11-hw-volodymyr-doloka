package org.geekhub.volodymyr.task2;

import java.util.List;
import java.util.Random;

public class Hw4main {
    public static void main(String[] args) {
        Course course = new Course();
        List<Mentor> mentors = course.getMentors();
        mentors.add(new Mentor("Ivanov Yuriy"));
        mentors.add(new Mentor("Sidorov Dmitriy"));
        mentors.add(new Mentor("Petrov Rostislav"));
        List<Student> students = course.getStudents();
        students.add(new Student("Vladimir Zelenkiy"));
        students.add(new Student("Svetlana Belenkaya"));
        students.add(new Student("Gennadiy Serenkiy"));
        students.add(new Student("Mikhail Kalkin"));
        students.add(new Student("Grigoriy Martin"));
        course.doLesson(1, "Introdusing Java");
        course.doLesson(2, "JDK & IDE");
        course.doLesson(3, "Variables");
        course.doLesson(4, "Classes & objects");
        course.doLesson(5, "Methods");
        course.doLesson(6, "Collections");
        final int LECTIONS_FINISHED = 6;
        for (Student student : course.getStudents()) {
            for (int lessonNumber = 1; lessonNumber < LECTIONS_FINISHED + 1; lessonNumber++) {
                Mentor randomMentor;
                randomMentor = course.getMentors().get(new Random().nextInt(3));
                course.checkHomework(randomMentor, student, lessonNumber);
            }
        }
        course.deletePassiveStudents();
        course.PrintStudentMaxSkippedLections();
        course.PrintMentorMaxMark5set();
    }
}
