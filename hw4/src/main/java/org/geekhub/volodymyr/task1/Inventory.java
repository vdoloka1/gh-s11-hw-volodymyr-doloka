package org.geekhub.volodymyr.task1;

import java.util.ArrayList;

public class Inventory {
    private ArrayList<Product> inventoryContent = new ArrayList<>();

    public void ShowContent() {
        System.out.println("Items: " + inventoryContent.size());
        System.out.println("Inventry content:");
        for (Product i : inventoryContent) {
            String itemName = i.getName();
            float itemPrice = i.getPrice();
            System.out.println(itemName + " " + i.getQuantityOnHand() + "Pcs, Price " + itemPrice);
        }
        System.out.println("Cost:");
        float inventoryCost = 0;
        for (Product i : inventoryContent) {
            inventoryCost += i.getQuantityOnHand() * i.getPrice();
        }
        System.out.println(inventoryCost);
    }

    public void addProduct(Product product) {
        inventoryContent.add(product);
    }
}
