package org.geekhub.volodymyr.task1;

public class Hw4main {
    public static void main(String[] args) {
        Inventory userInventory = new Inventory();
        userInventory.addProduct(new Product("Juice", 2.90f, 20));
        userInventory.addProduct(new Product("Milk", 1.50f, 50));
        userInventory.addProduct(new Product("Water", 0.50f, 100));
        userInventory.ShowContent();
    }
}
