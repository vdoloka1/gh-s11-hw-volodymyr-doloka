package org.geekhub.volodymyr.task1;

public class Product {
    private final String name;
    private final float price;
    private final int quantityOnHand;

    public Product(String name, float price, int quantityOnHand) {
        this.name = name;
        this.price = price;
        this.quantityOnHand = quantityOnHand;
    }

    public String getName() {
        return name;
    }

    public float getPrice() {
        return price;
    }

    public int getQuantityOnHand() {
        return quantityOnHand;
    }
}

