package org.geekhub.volodymyr.task2;

public class Car {
    private String model;
    private String color;
    private Type type;
    private Engine engine;
    private int maxSpeed;

    public Car(String model, String color, Type type, Engine engine, int maxSpeed) {
        this.model = model;
        this.color = color;
        this.type = type;
        this.engine = engine;
        this.maxSpeed = maxSpeed;

    }

    public Car() {
    }

    @Override
    public String toString() {
        return "Car{" +
                "model='" + model + '\'' +
                ", color='" + color + '\'' +
                ", type=" + type +
                ", engine=" + engine +
                ", maxSpeed=" + maxSpeed +
                '}';
    }
}
