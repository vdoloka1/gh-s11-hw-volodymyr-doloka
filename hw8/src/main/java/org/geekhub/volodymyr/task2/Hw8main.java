package org.geekhub.volodymyr.task2;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;

public class Hw8main {
    static Object cloneCreator(Object instance) {

        Class<?> clazz = instance.getClass();
        Object clone = null;
        try {
            clone = clazz.getDeclaredConstructor().newInstance();
        } catch (InstantiationException | NoSuchMethodException | InvocationTargetException | IllegalAccessException e) {
            e.printStackTrace();
        }
        Field[] fields = clazz.getDeclaredFields();
        for (Field field : fields) {
            field.setAccessible(true);
            try {
                field.set(clone, field.get(instance));
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
            return clone;
    }

    public static void main(String[] args) {
        Car myCar = new Car("bmw", "black", new Type(1996, "sedan", 2), new Engine("turbo", 15, 12), 300);
        System.out.println("original: " + myCar);
        System.out.println("Clone: " + cloneCreator(myCar));
    }
}