package org.geekhub.volodymyr.task2;

public class Engine {
    final private String name;
    final private int volume;
    final private int cilinders;

    public Engine(String name, int volume, int cilinders) {
        this.name = name;
        this.volume = volume;
        this.cilinders = cilinders;
    }

    @Override
    public String toString() {
        return "Engine{" +
                "name='" + name + '\'' +
                ", volume=" + volume +
                ", cilinders=" + cilinders +
                '}';
    }
}
