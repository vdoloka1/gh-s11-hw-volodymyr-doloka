package org.geekhub.volodymyr.task2;

public class Type {
    private final int year;
    private final String name;
    private final int modification;

    @Override
    public String toString() {
        return "Type{" +
                "year=" + year +
                ", name='" + name + '\'' +
                ", modification=" + modification +
                '}';
    }

    public Type(int year, String name, int modification) {
        this.year = year;
        this.name = name;
        this.modification = modification;

    }
}
