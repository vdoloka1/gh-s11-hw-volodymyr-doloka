package org.geekhub.volodymyr.task1;

public class Cat {
    private final String color;
    private final int age;
    @Ignore
    private final int legCount;
    private final int fullLength;

    public Cat(String color, int age, int legCount, int fullLength) {
        this.color = color;
        this.age = age;
        this.legCount = legCount;
        this.fullLength = fullLength;
    }
}
