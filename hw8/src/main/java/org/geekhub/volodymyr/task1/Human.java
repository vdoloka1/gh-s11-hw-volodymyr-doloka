package org.geekhub.volodymyr.task1;

public class Human {
    private final int height;
    private final String gender;
    private final int age;
    private final int weight;

    public Human(int height, String gender, int age, int weight) {
        this.height = height;
        this.gender = gender;
        this.age = age;
        this.weight = weight;
    }
}
