package org.geekhub.volodymyr.task1;

import java.lang.reflect.Field;

public class Hw8main {
    static void beanRepresenter(Object instance) {
        Class<?> clazz = instance.getClass();
        System.out.println("\n" + clazz.getSimpleName());
        Field[] fields = clazz.getDeclaredFields();
        for (Field field : fields) {
            if (field.isAnnotationPresent(Ignore.class)) {
                continue;
            } else {
                field.setAccessible(true);
                System.out.print(field.getName());
                Object value = null;
                try {
                    value = field.get(instance);
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
                System.out.println(": " + value);
            }
        }
    }

    public static void main(String[] args) {
        Cat cat = new Cat("black", 2, 3, 20);
        Car car = new Car("black negro", 300, "Sedan", "Cyber truck");
        Human human = new Human(2000, "Male", 33, 200);
        beanRepresenter(cat);
        beanRepresenter(car);
        beanRepresenter(human);
    }
}