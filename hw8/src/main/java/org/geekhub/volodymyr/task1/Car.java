package org.geekhub.volodymyr.task1;

public class Car {
    private final String color;
    private final int maxSpeed;
    private final String type;
    private final String model;

    public Car(String color, int maxSpeed, String type, String model) {
        this.color = color;
        this.maxSpeed = maxSpeed;
        this.type = type;
        this.model = model;
    }
}
