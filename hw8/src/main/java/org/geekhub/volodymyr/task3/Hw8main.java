package org.geekhub.volodymyr.task3;

import java.lang.reflect.Field;
import java.util.Objects;

public class Hw8main {
    static void beanComparator(Object object1, Object object2) {
        Class<?> clazz = object1.getClass();
        System.out.println("Comparing classes: " + clazz.getSimpleName());
        Field[] fields = clazz.getDeclaredFields();
        System.out.println("Fields:");
        for (Field field : fields) {
            boolean object1matched2 = false;
            if (field.isAnnotationPresent(Ignore.class)) {
                continue;
            } else {
                field.setAccessible(true);
                System.out.print(field.getName());
                Object object1FieldValue = null;
                Object object2FieldValue = null;
                try {
                    object1FieldValue = field.get(object1);
                    object2FieldValue = field.get(object2);
                    object1matched2 = Objects.equals(object1FieldValue, object2FieldValue);
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
                System.out.println(": " + object1FieldValue + " " + object2FieldValue + " Matched " + object1matched2);
            }
        }
    }

    public static void main(String[] args) {
        Car myCar = new Car(150, "Rx-7", "Sedan", "black", 300);
        Car otherCar = new Car(150, "Rx-9", "Coupe", "black", 300);
        beanComparator(myCar, otherCar);

    }
}