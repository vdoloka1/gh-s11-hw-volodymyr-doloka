package org.geekhub.volodymyr.task3;

public class Car {
    private final int maxCount;
    private final String model;
    private final String type;
    private final String color;
    @Ignore
    private final int maxSpeed;

    public Car(int maxCount, String model, String type, String color, int maxSpeed) {
        this.maxCount = maxCount;
        this.model = model;
        this.type = type;
        this.color = color;
        this.maxSpeed = maxSpeed;
    }
}
