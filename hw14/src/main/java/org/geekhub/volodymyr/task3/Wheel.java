package org.geekhub.volodymyr.task3;

public class Wheel {
    Tyre tyre;

    public Tyre getTyre() {
        return tyre;
    }

    public void setTyre(Tyre tyre) {
        this.tyre = tyre;
    }

    @Override
    public String toString() {
        return "Wheel{" +
                "tyre=" + tyre +
                '}';
    }
}
