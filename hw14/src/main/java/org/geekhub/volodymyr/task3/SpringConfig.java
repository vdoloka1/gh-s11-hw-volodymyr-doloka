package org.geekhub.volodymyr.task3;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

import java.util.ArrayList;
import java.util.List;

@Configuration
@ComponentScan("org.geekhub.volodymyr.task3")
public class SpringConfig {
    @Bean

    public Car car(Engine engine, List<Wheel> wheels) {
        Car car = new Car(engine, wheels);
        return car;
    }

    @Bean
    public Engine engine() {
        Engine engine = new Engine();
        engine.setCapacity(5000);
        return engine;
    }

    @Bean
    public Tyre Tyre() {
        Tyre tyre = new SummerTyre();
        tyre.setSize(15);
        tyre.setName("SummerJet");
        return tyre;
    }


    @Bean
    public Wheel wheel1(Tyre tyre) {
        Wheel wheel = new Wheel();
        wheel.setTyre(tyre);
        return wheel;
    }

    @Bean
    public Wheel wheel2(Tyre tyre) {
        Wheel wheel = new Wheel();
        wheel.setTyre(tyre);
        return wheel;
    }

    @Bean
    public Wheel wheel3(Tyre tyre) {
        Wheel wheel = new Wheel();
        wheel.setTyre(tyre);
        return wheel;
    }

    @Bean
    public Wheel wheel4(Tyre tyre) {
        Wheel wheel = new Wheel();
        wheel.setTyre(tyre);
        return wheel;
    }
}
