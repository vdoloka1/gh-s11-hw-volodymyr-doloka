package org.geekhub.volodymyr.task3;

import java.util.List;

public class Car {
    Engine engine;
    List<Wheel> wheels;
    public Car(Engine engine, List<Wheel> wheels) {
        this.engine = engine;
        this.wheels = wheels;
    }

    @Override
    public String toString() {
        return "Car{" +
                "engine=" + engine +
                ", wheels=" + wheels +
                '}';
    }
}
