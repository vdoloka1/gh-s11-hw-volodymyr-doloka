package org.geekhub.volodymyr.task3;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class hw14main {
    public static void main(String[] args) {
        final AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(SpringConfig.class);
        Car car = context.getBean("car", Car.class);
        System.out.println(car);
    }
}