package org.geekhub.volodymyr.task2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import java.util.ArrayList;
import java.util.List;

@Component
public class Car {
    Engine engine;
    List<Wheel> wheels;

    @Autowired
    public Car(Engine engine, Wheel wheel1,Wheel wheel2,Wheel wheel3,Wheel wheel4) {
        this.engine = engine;
        List<Wheel> wheels= new ArrayList<>();
        wheels.add(wheel1);
        wheels.add(wheel2);
        wheels.add(wheel3);
        wheels.add(wheel4);
        this.wheels = wheels;
    }

    @Override
    public String toString() {
        return "Car{" +
                "engine=" + engine +
                ", wheels=" + wheels +
                '}';
    }
}
