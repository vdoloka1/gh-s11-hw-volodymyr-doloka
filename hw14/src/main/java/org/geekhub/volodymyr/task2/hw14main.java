package org.geekhub.volodymyr.task2;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class hw14main {
    public static void main(String[] args) {
        final ApplicationContext context = new ClassPathXmlApplicationContext("applicationContextTask2.xml");
        Car car = context.getBean("car", Car.class);
        System.out.println(car);
    }


}
