package org.geekhub.volodymyr.task2;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;


@Component
public class SummerTyre extends Tyre {

    @Override
    public String getName() {
        return super.getName();
    }

    @Value("SummerJet")
    @Override
    public void setName(String name) {
        super.setName(name);
    }

    @Override

    public int getSize() {
        return super.getSize();
    }

    @Value("15")
    @Override
    public void setSize(int size) {
        super.setSize(size);
    }

    @Override
    public String toString() {
        return super.toString();
    }

}
