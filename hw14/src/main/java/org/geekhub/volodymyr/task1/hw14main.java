package org.geekhub.volodymyr.task1;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class hw14main {
    public static void main(String[] args) {
        final ApplicationContext context = new ClassPathXmlApplicationContext("applicationContextTask1.xml");
        Car car = context.getBean("car", Car.class);
        System.out.println(car);
    }
}
