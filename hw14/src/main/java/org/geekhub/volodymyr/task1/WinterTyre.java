package org.geekhub.volodymyr.task1;

public class WinterTyre extends Tyre {
    @Override
    public String getName() {
        return super.getName();
    }

    @Override
    public void setName(String name) {
        super.setName(name);
    }

    @Override
    public int getSize() {
        return super.getSize();
    }

    @Override
    public void setSize(int size) {
        super.setSize(size);
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
