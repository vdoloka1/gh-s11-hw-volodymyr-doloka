package org.geekhub.volodymyr;

public class Customer {
    private final int id;
    private final String first_name;
    private final String last_name;
    private final String cell_phone;

    public int getId() {
        return id;
    }

    public String getFirst_name() {
        return first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public String getCell_phone() {
        return cell_phone;
    }

    public Customer(int id, String first_name, String last_name, String cell_phone) {
        this.id = id;
        this.first_name = first_name;
        this.last_name = last_name;
        this.cell_phone = cell_phone;

    }
}
