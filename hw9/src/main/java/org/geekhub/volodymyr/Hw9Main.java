package org.geekhub.volodymyr;

import java.math.BigDecimal;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class Hw9Main {
    private static void deleteAllFromDB(Connection conn) throws SQLException {
        try {
            String sql = "DELETE FROM order_items;" +
                    "DELETE From \"order\";" +
                    "DELETE From customer;" +
                    "DELETE From product";
            PreparedStatement preparedStatement = conn.prepareStatement(sql);
            preparedStatement.executeUpdate();
        } catch (Exception ex) {
            throw ex;
        }
    }

    private static void addCustomersToDB(Connection conn) throws SQLException {
        try {
            String sql = "INSERT INTO customer (id, first_name,last_name,cell_phone) VALUES (?,?,?,?)";
            PreparedStatement preparedStatement = conn.prepareStatement(sql);
            List<Customer> customerList = new ArrayList<>();
            customerList.add(new Customer(1, "Ivanov", " Yuriy", "0675046221"));
            customerList.add(new Customer(2, "Sidorov", " Dmitriy", "0935046222"));
            customerList.add(new Customer(3, "Petrov", "Rostislav", "0955046223"));
            for (Customer customer : customerList
            ) {
                preparedStatement.setInt(1, customer.getId());
                preparedStatement.setString(2, customer.getFirst_name());
                preparedStatement.setString(3, customer.getLast_name());
                preparedStatement.setString(4, customer.getCell_phone());
                preparedStatement.executeUpdate();
            }
        } catch (Exception ex) {
            throw ex;
        }
    }

    private static void updateCustomerPhoneToDB(Connection conn) throws SQLException {
        try {
            String sql = "UPDATE customer SET cell_phone=? WHERE id=?";
            PreparedStatement preparedStatement = conn.prepareStatement(sql);
            preparedStatement.setString(1, "0677777777");
            preparedStatement.setInt(2, 1);
            preparedStatement.executeUpdate();
        } catch (Exception ex) {
            throw ex;
        }
    }

    private static void addProductsToDB(Connection conn) throws SQLException {
        try {
            String sql = "INSERT INTO product (id, name,description,current_price) VALUES (?,?,?,?)";
            PreparedStatement preparedStatement = conn.prepareStatement(sql);
            List<Product> productList = new ArrayList<>();
            productList.add(new Product(1, "Wiski", "Hard drink", 90.00));
            productList.add(new Product(2, "Cola", " Light drink", 25.50));
            productList.add(new Product(3, "Water", "Morning drink", 11.99));
            for (Product product : productList
            ) {
                preparedStatement.setInt(1, product.getId());
                preparedStatement.setString(2, product.getName());
                preparedStatement.setString(3, product.getDescription());
                preparedStatement.setBigDecimal(4, BigDecimal.valueOf(product.getCurrent_price()));
                preparedStatement.executeUpdate();
            }
        } catch (Exception ex) {
            throw ex;
        }
    }

    private static void addOrdersToDB(Connection conn) throws SQLException {
        try {
            List<Order> orderList = new ArrayList<>();
            ArrayList<OrderItem> order1Items = new ArrayList<>();
            order1Items.add(new OrderItem(1, 90.00, 2));
            order1Items.add(new OrderItem(2, 25.50, 3));
            ArrayList<OrderItem> order2Items = new ArrayList<>();
            order2Items.add(new OrderItem(1, 90.00, 1));
            order2Items.add(new OrderItem(2, 25.50, 3));
            order2Items.add(new OrderItem(3, 11.99, 3));
            ArrayList<OrderItem> order3Items = new ArrayList<>();
            order3Items.add(new OrderItem(2, 25.50, 1));
            order3Items.add(new OrderItem(3, 11.99, 1));
            orderList.add(new Order(1, 1, order1Items, "Cherkassy.Shevhenko st.25"));
            orderList.add(new Order(2, 1, order2Items, "Cherkassy.Shevhenko st.27"));
            orderList.add(new Order(3, 2, order3Items, "Cherkassy.Shevhenko st.26"));
            String sql = "INSERT INTO \"order\" (id,customer_id,delivery_place) VALUES (?,?,?)";
            PreparedStatement psOrder = conn.prepareStatement(sql);
            String sqlItems = "INSERT INTO order_items (order_id,product_id,product_price,product_quantity) VALUES (?,?,?,?)";
            PreparedStatement psOrderItems = conn.prepareStatement(sqlItems);
            for (Order order : orderList
            ) {
                psOrder.setInt(1, order.getId());
                psOrder.setInt(2, order.getCustomer_id());
                psOrder.setString(3, order.getDelivery_place());
                psOrder.executeUpdate();
                for (OrderItem orderItem : order.getOrderItem()
                ) {
                    psOrderItems.setInt(1, order.getId());
                    psOrderItems.setInt(2, orderItem.getProduct_id());
                    psOrderItems.setBigDecimal(3, BigDecimal.valueOf(orderItem.getProduct_price()));
                    psOrderItems.setInt(4, orderItem.getProduct_quantity());
                    psOrderItems.executeUpdate();
                }
            }
        } catch (Exception ex) {
            throw ex;
        }
    }

    private static void selectAnaliticsFromDB(Connection conn) throws SQLException {
        try {
            String sqlSelectOrderSum = "select customer_id,first_name,last_name,cell_phone,sum (oi.product_price*oi.product_quantity) " +
                    "FROM customer join \"order\" o on customer.id = o.customer_id\n" +
                    "JOIN order_items oi on o.id = oi.order_id\n" +
                    "GROUP BY customer_id,first_name,last_name,cell_phone";
            PreparedStatement psOrdersSum = conn.prepareStatement(sqlSelectOrderSum);
            ResultSet rsOrdersSum = psOrdersSum.executeQuery();
            while (rsOrdersSum.next()) {
                int id = rsOrdersSum.getInt("customer_id");
                String firstName = rsOrdersSum.getString(2);
                String lastName = rsOrdersSum.getString("last_name");
                int phone = rsOrdersSum.getInt("cell_phone");
                String orderSum = rsOrdersSum.getString("sum");
                System.out.println("Max orders sum:");
                System.out.println("ID:" + id + " Name:" + firstName + lastName + "" + " Phone:" + phone + " Sum:" + orderSum);
            }
            String SqlSelectMaxOrderedProduct = "SELECT distinct product_id,name,sum(product_quantity) sum FROM order_items\n" +
                    "JOIN product p on p.id = order_items.product_id\n" +
                    "GROUP BY product_id, name\n" +
                    "ORDER BY sum DESC\n" +
                    "LIMIT 1";
            PreparedStatement preparedStatement = conn.prepareStatement(SqlSelectMaxOrderedProduct);
            ResultSet rsMaxOrderedProduct = preparedStatement.executeQuery();
            while (rsMaxOrderedProduct.next()) {
                int id = rsMaxOrderedProduct.getInt("product_id");
                String productName = rsMaxOrderedProduct.getString("name");
                String productMaxOrderedCount = rsMaxOrderedProduct.getString("sum");
                System.out.println("Max ordered product sum:");
                System.out.println("ID:" + id + "Product Name:" + productName + " count:" + productMaxOrderedCount);
            }
        } catch (Exception ex) {
            throw ex;
        }
    }

    public static void main(String[] args) {
        try {
            Connection conn = DriverManager.getConnection(
                    "jdbc:postgresql://localhost:5432/postgres",
                    "postgres",
                    "123547");
            deleteAllFromDB(conn);
            addCustomersToDB(conn);
            updateCustomerPhoneToDB(conn);
            addProductsToDB(conn);
            addOrdersToDB(conn);
            selectAnaliticsFromDB(conn);
        } catch (
                Exception ex) {
            System.out.println("Exeption: " + ex);
        }
    }
}


