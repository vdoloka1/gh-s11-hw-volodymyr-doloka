package org.geekhub.volodymyr;

public class OrderItem {
    private final int product_id;
    private final double product_price;
    private final int product_quantity;

    public OrderItem(int product_id, double product_price, int product_quantity) {
        this.product_id = product_id;
        this.product_price = product_price;
        this.product_quantity = product_quantity;
    }

    public int getProduct_id() {
        return product_id;
    }

    public double getProduct_price() {
        return product_price;
    }

    public int getProduct_quantity() {
        return product_quantity;
    }
}
