package org.geekhub.volodymyr;

public class Product {
    private final int id;
    private final String name;
    private final String description;
    private final double current_price;

    public Product(int id, String name, String description, double current_price) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.current_price = current_price;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public double getCurrent_price() {
        return current_price;
    }
}

