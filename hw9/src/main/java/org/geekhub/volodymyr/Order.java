package org.geekhub.volodymyr;

import java.util.ArrayList;

public class Order {
    private final int id;
    private final int customer_id;
    private final ArrayList<OrderItem> OrderItem;
    private final String delivery_place;

    public Order(int id, int customer_id, ArrayList<OrderItem> orderItem, String delivery_place) {
        this.id = id;
        this.customer_id = customer_id;
        OrderItem = orderItem;
        this.delivery_place = delivery_place;
    }

    public int getId() {
        return id;
    }

    public int getCustomer_id() {
        return customer_id;
    }

    public ArrayList<org.geekhub.volodymyr.OrderItem> getOrderItem() {
        return OrderItem;
    }

    public String getDelivery_place() {
        return delivery_place;
    }
}
