1.
SELECT countries.id, sortname, countries.name, phonecode, count(country_id) cnt
FROM countries
         JOIN states s ON countries.id = s.country_id
GROUP BY countries.id
ORDER BY cnt DESC LIMIT 1
2.
SELECT countries.id, sortname, countries.name, phonecode, count(country_id) cnt
FROM countries
         JOIN states s ON countries.id = s.country_id
         JOIN cities c ON s.id = c.state_id
GROUP BY countries.id
ORDER BY cnt DESC LIMIT 1
3.
SELECT c.id, sortname, c.name, phonecode, count(country_id) cnt
FROM countries c
         JOIN states s ON c.id = s.country_id
GROUP BY c.id
ORDER BY cnt DESC, c.name, c.id
    4.
SELECT c.id, sortname, c.name, phonecode, count(country_id) cnt
FROM countries c
         JOIN states s ON c.id = s.country_id
         JOIN cities ci ON s.id = ci.state_id
GROUP BY c.id
ORDER BY cnt DESC, c.name, c.id
    5.
SELECT c.id, sortname, c.name, phonecode, count(country_id), count(DISTINCT state_id) cnt
FROM countries c
         JOIN states s ON c.id = s.country_id
         JOIN cities ci ON s.id = ci.state_id
GROUP BY c.id
ORDER BY cnt DESC
    6.
SELECT DISTINCT c.id, sortname, c.name, phonecode, count(s.id) cnt
FROM countries c
         JOIN states s ON c.id = s.country_id
         JOIN cities ci ON s.id = ci.state_id
GROUP BY s.id, c.id
ORDER BY cnt DESC, c.id LIMIT 10
7.
SELECT id, sortname, name, phonecode, cnt
FROM ((SELECT s.country_id, count(s.country_id) cnt
       FROM states s
       GROUP BY s.country_id
       ORDER BY cnt DESC LIMIT 10)
      UNION ALL
      (SELECT s.country_id, count(s.country_id) cnt
       FROM states s
       GROUP BY s.country_id
       ORDER BY cnt LIMIT 10)) ccnt
         JOIN countries ON id = ccnt.country_id
ORDER BY name
    8.
SELECT id, sortname, name, phonecode, cnt
FROM (SELECT s.country_id, count(s.country_id) cnt, avg(count(s.country_id)) OVER () avgc
      FROM states s
      GROUP BY s.country_id
      ORDER BY cnt DESC
     ) ccnt
         JOIN countries ON id = ccnt.country_id
GROUP BY id, sortname, name, phonecode, cnt, avgc
HAVING cnt > avgc
    9.
SELECT DISTINCT
ON (cnt) id, sortname, name, phonecode, cnt
FROM (SELECT s.country_id, count (s.country_id) cnt
    FROM states s
    GROUP BY s.country_id
    ORDER BY cnt DESC )
    ccnt
    JOIN countries
ON id = ccnt.country_id
ORDER BY cnt, name
    10.
SELECT s.name
FROM states s
GROUP BY s.name
HAVING (count(s.name) > 1)
    11.
SELECT s.id, s.name, s.country_id
FROM states s
         left join cities c on c.state_id = s.id
where c.id IS NULL