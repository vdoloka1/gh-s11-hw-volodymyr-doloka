CREATE TABLE IF NOT EXISTS "customer"
(
    "id"         SERIAL      NOT NULL,
    "first_name" VARCHAR(50) NOT NULL,
    "last_name"  VARCHAR(50) NOT NULL,
    "cell_phone" VARCHAR(10) NOT NULL,
    PRIMARY KEY ("id")
);

CREATE TABLE IF NOT EXISTS "product"
(
    "id"            SERIAL       NOT NULL,
    "name"          VARCHAR(100) NOT NULL,
    "description"   VARCHAR(500) NOT NULL,
    "current_price" MONEY        NOT NULL,
    PRIMARY KEY ("id")
);

CREATE TABLE IF NOT EXISTS "order"
(
    "id"             SERIAL       NOT NULL,
    "customer_id"    INTEGER      NOT NULL REFERENCES customer (id),
    "delivery_place" VARCHAR(200) NOT NULL,
    PRIMARY KEY ("id")
);

CREATE TABLE IF NOT EXISTS "order_items"
(
    "id"               SERIAL  NOT NULL,
    "order_id"         INTEGER NOT NULL REFERENCES "order" (id),
    "product_id"       INTEGER NOT NULL REFERENCES product (id),
    "product_price"    MONEY   NOT NULL,
    "product_quantity" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);