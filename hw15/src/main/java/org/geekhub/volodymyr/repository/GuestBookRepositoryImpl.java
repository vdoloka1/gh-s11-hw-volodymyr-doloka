package org.geekhub.volodymyr.repository;

import java.util.concurrent.CopyOnWriteArrayList;

public class GuestBookRepositoryImpl implements GuestBookRepository {

    @Override
    public void addEntry(GuestBookEntry guestBookEntry) {
        GuestBookDB.getEntries().add(guestBookEntry);
    }

    @Override
    public CopyOnWriteArrayList<GuestBookEntry> getEntries() {
        return GuestBookDB.getEntries();
    }
}
