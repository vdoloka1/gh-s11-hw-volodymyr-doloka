package org.geekhub.volodymyr.repository;

import java.util.concurrent.CopyOnWriteArrayList;

public interface GuestBookRepository {

    void addEntry(GuestBookEntry guestBookEntry);

    CopyOnWriteArrayList<GuestBookEntry> getEntries();
}
