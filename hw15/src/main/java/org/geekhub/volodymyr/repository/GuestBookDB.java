package org.geekhub.volodymyr.repository;

import java.util.concurrent.CopyOnWriteArrayList;

public final class GuestBookDB {
    static CopyOnWriteArrayList<GuestBookEntry> entries = new CopyOnWriteArrayList<>();

    public static CopyOnWriteArrayList<GuestBookEntry> getEntries() {
        return entries;
    }
}
