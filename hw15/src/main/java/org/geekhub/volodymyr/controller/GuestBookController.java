package org.geekhub.volodymyr.controller;

import org.geekhub.volodymyr.repository.GuestBookEntry;
import org.geekhub.volodymyr.repository.GuestBookRepositoryImpl;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.annotation.PostConstruct;
import java.time.LocalDateTime;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Controller
public class GuestBookController {

    private GuestBookRepositoryImpl guestBookRepository;

    @GetMapping("/guestbook")
    public String getEntries(@RequestParam(name = "page", required = false, defaultValue = "1") int page,
                             @RequestParam(name = "itemPerPage", required = false, defaultValue = "10") int itemPerPage,
                             Model model) {
        List<GuestBookEntry> entries = guestBookRepository.getEntries();
        List<GuestBookEntry> selectedEntries = entries.stream()
                .sorted(Comparator.reverseOrder())
                .skip((page - 1) * itemPerPage)
                .limit(itemPerPage)
                .collect(Collectors.toList());
        model.addAttribute("entries", selectedEntries);
        int pages = (int) Math.ceil((float) entries.size() / (float) itemPerPage);
        model.addAttribute("pages", pages);
        model.addAttribute("page", page);
        model.addAttribute("itemPerPage", itemPerPage);
        return "index";
    }

    @PostMapping("/guestbook")
    public String formPost(@RequestParam(name = "name", required = false, defaultValue = "Unknown") String name,
                           @RequestParam(name = "message", required = false, defaultValue = "") String message,
                           @RequestParam(name = "rating", required = false, defaultValue = "5") String rating) {
        int mark = Integer.parseInt(rating);
        LocalDateTime date = LocalDateTime.now();
        GuestBookEntry guestBookEntry = new GuestBookEntry(name, message, mark, date);
        guestBookRepository.addEntry(guestBookEntry);
        return "redirect:/guestbook";
    }

    @PostConstruct
    public void initRepositoryEntries() {
        guestBookRepository = new GuestBookRepositoryImpl();
        for (int i = 1; i < 101; i++) {
            String name = "name" + i;
            String message = "message" + i;
            int mark = (int) Math.floor(Math.random() * 5 + 1);
            LocalDateTime date = LocalDateTime.now().minusSeconds(i);
            GuestBookEntry guestBookEntry = new GuestBookEntry(name, message, mark, date);
            guestBookRepository.addEntry(guestBookEntry);
        }
    }
}