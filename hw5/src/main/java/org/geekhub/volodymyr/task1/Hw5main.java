package org.geekhub.volodymyr.task1;

import java.time.LocalDate;
import java.time.LocalDateTime;

public class Hw5main {
    public static void main(String[] args) {
        TaskManager sheduler = new TaskManagerImpl();
        LocalDateTime taskDate;
        taskDate = LocalDateTime.of(2021, 12, 1, 14, 15);
        Task userTask = new Task("taskname1", "Category 1");
        sheduler.add(taskDate, userTask);
        taskDate = LocalDateTime.of(2021, 12, 2, 14, 15);
        userTask = new Task("taskname2", "Category 2");
        sheduler.add(taskDate, userTask);
        userTask = new Task("taskname3", "Category 2");
        taskDate = LocalDateTime.of(2021, 12, 2, 14, 15);
        sheduler.add(taskDate, userTask);
        userTask = new Task("taskname4", "Category 3");
        taskDate = LocalDateTime.of(2022, 1, 2, 1, 15);
        sheduler.add(taskDate, userTask);
        sheduler.remove(LocalDateTime.of(2022, 1, 2, 1, 15));
        userTask = new Task("taskname5", "Category 3");
        taskDate = LocalDateTime.now();
        sheduler.add(taskDate, userTask);
        userTask = new Task("taskname6", "Category 2");
        taskDate = LocalDateTime.now();
        sheduler.add(taskDate, userTask);
        System.out.println("Task for today " + LocalDate.now());
        System.out.println(sheduler.getTasksForToday());
        System.out.println("Categoties list:" + sheduler.getCategories());
        System.out.println("TaskByCategory: " + sheduler.getTasksByCategory("Category 2"));
        System.out.println("TaskByCategories: " + sheduler.getTasksByCategories("Category 2", "Category 3"));
    }
}
