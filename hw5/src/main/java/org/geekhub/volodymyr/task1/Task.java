package org.geekhub.volodymyr.task1;

public class Task {

    final private String name;
    final private String category;

    @Override
    public String toString() {
        return "name='" + name + '\'' +
                ", category='" + category + '\'';
    }

    public Task(String name, String category) {
        this.name = name;
        this.category = category;

    }

    public String getCategory() {
        return category;
    }
}
