package org.geekhub.volodymyr.task1;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;

public class TaskManagerImpl implements TaskManager {

    private final Map<LocalDateTime, List<Task>> sheduleMap = new TreeMap<>();

    @Override
    public void add(LocalDateTime date, Task task) {
        List<Task> taskList;
        if (sheduleMap.containsKey(date)) {
            taskList = sheduleMap.get(date);
        } else {
            taskList = new LinkedList<>();
        }
        taskList.add(task);
        sheduleMap.put(date, taskList);
    }

    @Override
    public void remove(LocalDateTime date) {
        sheduleMap.remove(date);
    }

    @Override
    public Set<String> getCategories() {
        Set<String> categories = new TreeSet<>();
        for (Map.Entry<LocalDateTime, List<Task>> entry : sheduleMap.entrySet()) {
            List<Task> taskList;
            taskList = entry.getValue();
            for (Task task : taskList) {
                categories.add(task.getCategory());
            }
        }
        return categories;
    }

    @Override
    public Map<String, List<Task>> getTasksByCategories(String... categories) {
        Map<String, List<Task>> categoriesMap = new HashMap<>();
        for (String category : categories
        ) {
            categoriesMap.put(category, getTasksByCategory(category));
        }
        return categoriesMap;
    }

    @Override
    public List<Task> getTasksByCategory(String category) {
        LinkedList<Task> taskListByCategory = new LinkedList<>();
        for (Map.Entry<LocalDateTime, List<Task>> entry : sheduleMap.entrySet()) {
            List<Task> taskList;
            taskList = entry.getValue();
            for (Task task : taskList) {
                if (task.getCategory().equals(category)) {
                    taskListByCategory.add(task);
                }
            }
        }
        return taskListByCategory;
    }

    @Override
    public List<Task> getTasksForToday() {
        LinkedList<Task> taskList = new LinkedList<>();
        for (Map.Entry<LocalDateTime, List<Task>> entry : sheduleMap.entrySet()) {
            if (Objects.equals(entry.getKey().toLocalDate(), LocalDate.now())) {
                taskList.addAll(entry.getValue());
            }
        }
        return taskList;
    }
}
