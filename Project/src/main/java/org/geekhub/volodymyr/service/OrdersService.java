package org.geekhub.volodymyr.service;

import org.geekhub.volodymyr.DTO.HubOrderDTO;
import org.geekhub.volodymyr.entity.OrderEntity;

import java.util.List;

public interface OrdersService {

    void addOrder(OrderEntity orderEntity);

    void confirmOrder(int orderId);

    List<OrderEntity> getOrders(int Page, int itemPerPage);

    List<HubOrderDTO> getHubOrders(int Page, int itemPerPage);

    List<HubOrderDTO> getConfirmedOrders(int page, int itemPerPage);
}