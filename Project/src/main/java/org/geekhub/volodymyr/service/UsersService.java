package org.geekhub.volodymyr.service;

import org.geekhub.volodymyr.entity.UserEntity;

public interface UsersService {

    void addUser(UserEntity userEntity);

    void updateUser(UserEntity userEntity);

    UserEntity findByUserID(int id);
}