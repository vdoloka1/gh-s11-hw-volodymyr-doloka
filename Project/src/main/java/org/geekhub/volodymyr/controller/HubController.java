package org.geekhub.volodymyr.controller;

import org.geekhub.volodymyr.entity.LocationEntity;
import org.geekhub.volodymyr.entity.UserEntity;
import org.geekhub.volodymyr.repository.impl.LocationsRepositoryImpl;
import org.geekhub.volodymyr.service.impl.UsersServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@Controller
public class HubController {

    private final UsersServiceImpl usersService;
    private final LocationsRepositoryImpl locationsRepository;

    @Autowired
    public HubController(UsersServiceImpl usersService, LocationsRepositoryImpl locationsRepository) {
        this.usersService = usersService;
        this.locationsRepository = locationsRepository;
    }

//    @GetMapping("/hub")
    @RequestMapping(value = {"/hub/{hubId}"}, method = RequestMethod.GET)

    public String viewProfile(@PathVariable(value = "hubId") int hubId,Model model) {
        List<LocationEntity> locations = locationsRepository.getLocations();
        model.addAttribute("locations", locations);
        UserEntity userEntity=usersService.findByUserID(hubId);
        model.addAttribute("hub", userEntity);
        return "hub";
    }
}