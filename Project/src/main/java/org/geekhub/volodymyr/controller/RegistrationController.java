package org.geekhub.volodymyr.controller;

import org.geekhub.volodymyr.entity.LocationEntity;
import org.geekhub.volodymyr.entity.UserEntity;
import org.geekhub.volodymyr.repository.impl.LocationsRepositoryImpl;
import org.geekhub.volodymyr.service.impl.UsersServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.List;


@Controller
public class RegistrationController {

    private final UsersServiceImpl usersService;
    private final LocationsRepositoryImpl locationsRepository;

    @Autowired
    public RegistrationController(UsersServiceImpl usersService, LocationsRepositoryImpl locationsRepository) {
        this.usersService = usersService;
        this.locationsRepository = locationsRepository;
    }

    @GetMapping("/registration")
    public String registration(Model model) {
        List<LocationEntity> locations = locationsRepository.getLocations();
        model.addAttribute("locations", locations);
        return "registration";
    }

    @PostMapping("/registration")
    public String registerUser(UserEntity userEntity, Model model) {
        usersService.addUser(userEntity);
        model.addAttribute("message", "Registration Success");
        return "login";
    }
}