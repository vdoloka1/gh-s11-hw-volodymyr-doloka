package org.geekhub.volodymyr.repository;

import org.geekhub.volodymyr.entity.LocationEntity;

import java.util.List;

public interface LocationsRepository {

    List<LocationEntity> getLocations();

}