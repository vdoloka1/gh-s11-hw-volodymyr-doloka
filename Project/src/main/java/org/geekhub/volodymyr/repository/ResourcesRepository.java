package org.geekhub.volodymyr.repository;

import org.geekhub.volodymyr.entity.CategoryEntity;
import org.geekhub.volodymyr.entity.ResourceEntity;
import org.geekhub.volodymyr.entity.SubCategoryEntity;

import java.util.List;

public interface ResourcesRepository {

    List<CategoryEntity> getCategories();

    List<SubCategoryEntity> getSubCategoriesByCategorie(int categorieId);

    List<ResourceEntity> getResourcesBySubcategory(int subCategoryId);
}