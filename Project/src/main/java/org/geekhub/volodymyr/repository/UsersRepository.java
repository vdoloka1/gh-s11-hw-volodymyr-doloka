package org.geekhub.volodymyr.repository;

import org.geekhub.volodymyr.entity.UserEntity;


public interface UsersRepository {
    void addUser(UserEntity userEntity);

    void updateUser(UserEntity userEntity);

    UserEntity findByUsername(String username);

    UserEntity findByUserID(int id);
}