package org.geekhub.volodymyr.repository;

import org.geekhub.volodymyr.DTO.HubOrderDTO;
import org.geekhub.volodymyr.entity.HubEntity;
import org.geekhub.volodymyr.entity.OrderEntity;

import java.util.List;

public interface OrdersRepository {

    void addOrder(OrderEntity orderEntity);

    void confirmOrder(int orderId);

    List<OrderEntity> getOrders(int Page, int itemPerPage);

    List<HubOrderDTO> getHubOrders(int Page, int itemPerPage);

    List<HubOrderDTO> getConfirmedOrders(int page, int itemPerPage);

    List<HubEntity> getLackResources(int page, int itemPerPage);

    List<HubEntity> getCountOrderingResources(int page, int itemPerPage);
}