package org.geekhub.volodymyr.task1;

class Triangle extends Shape {
    double sideA;
    double sideB;
    double sideC;

    @Override
    double calculateArea() {
        double halfPerimeter = (sideA + sideB + sideC) / 2;
        return Math.sqrt(halfPerimeter * (halfPerimeter - sideA) * (halfPerimeter - sideB) * (halfPerimeter - sideC));
    }

    @Override
    double calculatePerimeter() {
        return sideA + sideB + sideC;
    }

    public Triangle(double sideA, double sideB, double sideC) {
        this.sideA = sideA;
        this.sideB = sideB;
        this.sideC = sideC;
    }
}
