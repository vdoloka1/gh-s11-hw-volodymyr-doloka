package org.geekhub.volodymyr.task1;

abstract class Shape {
    abstract double calculateArea();

    abstract double calculatePerimeter();

}
