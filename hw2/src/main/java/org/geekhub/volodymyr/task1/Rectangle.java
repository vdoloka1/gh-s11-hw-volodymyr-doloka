package org.geekhub.volodymyr.task1;

class Rectangle extends Shape {
    Double sideA;
    Double sideB;

    @Override
    double calculateArea() {
        return sideA * sideB;
    }

    @Override
    double calculatePerimeter() {
        return (sideA + sideB) * 2;
    }

    protected Rectangle(Double sideA, Double sideB) {
        this.sideA = sideA;
        this.sideB = sideB;

    }
}
