package org.geekhub.volodymyr.task1;

class Circle extends Shape {
    Double radius;

    @Override
    double calculateArea() {
        return Math.PI * radius * radius;
    }

    double calculatePerimeter() {
        return 2 * Math.PI * this.radius;
    }

    public Circle(Double radius) {
        this.radius = radius;
    }
}
