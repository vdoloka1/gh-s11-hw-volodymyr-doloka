package org.geekhub.volodymyr.task1;

class Square extends Shape {
    double sideA;

    @Override
    double calculateArea() {
        return sideA * sideA;
    }

    @Override
    double calculatePerimeter() {
        return sideA * 4;
    }

    protected Square(double sideA) {
        this.sideA = sideA;
    }
}
