package org.geekhub.volodymyr.task2;

class Circle implements Shape {
    Double radius;

    @Override
    public double calculateArea() {
        return Math.PI * radius * radius;
    }

    public double calculatePerimeter() {
        return 2 * Math.PI * radius;
    }

    protected Circle(Double radius) {
        this.radius = radius;
    }

}
