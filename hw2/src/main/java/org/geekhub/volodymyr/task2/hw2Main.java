package org.geekhub.volodymyr.task2;


import java.util.Scanner;

import static java.lang.System.exit;

public class hw2Main {
    private static void showCompositeShape(double sideA, double sideB) {
        System.out.println("Comprise of 2 triangles with sides:" + "A=" + sideA + " B=" + sideB + " C="
                + Math.sqrt(sideA * sideA + sideB * sideB));
    }

    private static String InputShapeType() {
        System.out.println("Please enter shape type (e.g. Circle, Square, Rectangle, Triangle number): ");
        Scanner input = new Scanner(System.in);
        String shapeType = input.nextLine();
        shapeType = shapeType.toUpperCase();
        return shapeType;
    }

    private static double InputRadius() {
        System.out.println("Please enter radius: ");
        Scanner input = new Scanner(System.in);
        return input.nextDouble();
    }

    private static double InputSide(String sidename) {
        System.out.println("Please enter side " + sidename + ":");
        Scanner input = new Scanner(System.in);
        return input.nextDouble();
    }

    public static void main(String[] args) {

        ShapeType shapeType = ShapeType.valueOf(InputShapeType());
        Shape userShape = null;
        switch (shapeType) {
            case CIRCLE: {
                System.out.println("Circle selected");
                userShape = new Circle(InputRadius());
            }
            break;
            case SQUARE: {
                System.out.println("Square selected");
                Double sideA = InputSide("");
                userShape = new Square(sideA);
                showCompositeShape(sideA, sideA);
            }
            break;
            case RECTANGLE: {
                System.out.println("Rectangle selected");
                Double sideA = InputSide("A");
                Double sideB = InputSide("B");
                userShape = new Rectangle(sideA, sideB);
                showCompositeShape(sideA, sideB);
            }
            break;
            case TRIANGLE:
                System.out.println("Triangle selected");
                Double sideA = InputSide("A");
                Double sideB = InputSide("B");
                Double sideC = InputSide("C");
                if (sideA + sideB < sideC | sideB + sideC < sideA | sideA + sideC < sideB) {
                    System.out.println("Triangle not exist");
                    exit(30);
                }
                userShape = new Triangle(sideA, sideB, sideC);
                break;
        }
        System.out.println("Perimeter: " + userShape.calculatePerimeter());
        System.out.println("Area: " + userShape.calculateArea());
    }
}
