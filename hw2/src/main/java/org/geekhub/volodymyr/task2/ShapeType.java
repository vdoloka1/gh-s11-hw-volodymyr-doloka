package org.geekhub.volodymyr.task2;

public enum ShapeType {
    CIRCLE,
    SQUARE,
    RECTANGLE,
    TRIANGLE
}
