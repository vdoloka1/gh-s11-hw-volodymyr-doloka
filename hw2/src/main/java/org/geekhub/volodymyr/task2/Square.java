package org.geekhub.volodymyr.task2;

class Square implements Shape {
    double sideA;

    @Override
    public double calculateArea() {
        return sideA * sideA;
    }

    @Override
    public double calculatePerimeter() {
        return sideA * 4;
    }

    protected Square(double sideA) {
        this.sideA = sideA;
    }
}
