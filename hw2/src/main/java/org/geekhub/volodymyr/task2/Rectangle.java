package org.geekhub.volodymyr.task2;

class Rectangle implements Shape {
    Double sideA;
    Double sideB;

    @Override
    public double calculateArea() {
        return sideA * sideB;
    }

    @Override
    public double calculatePerimeter() {
        return (sideA + sideB) * 2;
    }

    protected Rectangle(Double sideA, Double sideB) {
        this.sideA = sideA;
        this.sideB = sideB;
    }
}
