package org.geekhub.volodymyr.task2;

interface Shape {
    double calculateArea();

    double calculatePerimeter();
}
