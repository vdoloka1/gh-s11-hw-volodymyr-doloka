package org.geekhub.volodymyr.task1;

public class Node<E> {
    protected Node<E> previous;
    private E content;
    protected Node<E> next;

    public E getContent() {
        return content;
    }

    public void setContent(E content) {
        this.content = content;
    }

    @Override
    public String toString() {
        return "Node{" +
                "content=" + content +
                '}';
    }
}
