package org.geekhub.volodymyr.task1;

import java.util.Comparator;
import java.util.function.Supplier;

public class Hw6main {
    public static void main(String[] args) {
        LinkedListCustom<String> myList;
        myList = new LinkedListCustom<>();
        myList.add("1 content");
        myList.add("2 content");
        myList.add("3 content");
        myList.add("4 content");
        myList.add("5 content");
        System.out.println("size " + myList.getSize());
        myList.printContent();
        myList.swapNodesByIndex(2, 4);
        System.out.println("Swapped" + 2 + " and " + 4);
        myList.printContent();
        Comparator<String> comparator = Comparator.comparingInt(String::length)
                .thenComparing(String.CASE_INSENSITIVE_ORDER);
        Supplier<LinkedListCustom<String>> listSupplier = LinkedListCustom::new;
        System.out.println("sorted bubles: ASC");
        myList.sortByBubbles(comparator, Direction.ASC, listSupplier).printContent();
        System.out.println("sorted bubles: DESC");
        myList.sortByBubbles(comparator, Direction.DESC, listSupplier).printContent();
        System.out.println("sorted insertions ASC:");
        myList.sortByInsertion(comparator, Direction.ASC, listSupplier).printContent();
        System.out.println("sorted insertions DESC:");
        myList.sortByInsertion(comparator, Direction.DESC, listSupplier).printContent();
        System.out.println("Unsorted:");
        myList.printContent();
    }
}