package org.geekhub.volodymyr.task1;

import java.util.Comparator;
import java.util.function.Supplier;

public class LinkedListCustom<E> {
    private Node<E> firstNode;
    private Node<E> lastNode;
    private int size = 0;

    public LinkedListCustom() {
    }

    private LinkedListCustom<E> getClone(Supplier<LinkedListCustom<E>> supplier) {
        LinkedListCustom<E> cloneList = supplier.get();
        cloneList.add(firstNode.getContent());
        Node<E> currentOriginalNode = firstNode.next;
        for (int i = 1; i < size; i++) {
            cloneList.add(currentOriginalNode.getContent());
            currentOriginalNode = currentOriginalNode.next;
        }
        return cloneList;
    }

    public int getSize() {
        return size;
    }

    public void add(E content) {
        if (size == 0) {
            Node<E> newNode;
            newNode = new Node<>();
            newNode.setContent(content);
            firstNode = newNode;
            lastNode = newNode;
            size++;
            return;
        }
        if (size > 0) {
            Node<E> newNode;
            newNode = new Node<>();
            newNode.setContent(content);
            newNode.previous = lastNode;
            lastNode.next = newNode;
            lastNode = newNode;
            if (size == 1) {
                firstNode.next = newNode;
            }
            size++;
        }
    }

    public void swapNodesByIndex(int indexFirstNode, int indexSecondNode) {
        if (indexFirstNode > size || indexSecondNode > size) {
            System.out.println("index out of bounds");
            return;
        }
        Node<E> temp1 = getNodeByIndex(indexFirstNode);
        Node<E> temp2 = getNodeByIndex(indexSecondNode);
        Node<E> temp = new Node<>();
        temp.setContent(temp1.getContent());
        temp1.setContent(temp2.getContent());
        temp2.setContent(temp.getContent());
    }

    private Node<E> getNodeByIndex(int index) {
        Node<E> currentNode = firstNode;
        int i = 1;
        while (i < index) {
            currentNode = currentNode.next;
            i++;
        }
        return currentNode;

    }

    public LinkedListCustom<E> sortByBubbles(Comparator<E> comparator, Direction direction, Supplier<LinkedListCustom<E>> supplier) {
        LinkedListCustom<E> sortedCollection = getClone(supplier);
        int j = sortedCollection.size - 1;
        while (j > 0) {
            int i = 1;
            Node<E> currentNode = sortedCollection.firstNode;
            while (i < j + 1) {
                if (direction.getDirection() * (comparator.compare(currentNode.getContent(), currentNode.next.getContent())) > 0) {
                    Node<E> temp = new Node<>();
                    temp.setContent(currentNode.getContent());
                    currentNode.setContent(currentNode.next.getContent());
                    currentNode.next.setContent(temp.getContent());
                }
                currentNode = currentNode.next;
                i++;
            }
            j--;
        }
        return sortedCollection;
    }

    public LinkedListCustom<E> sortByInsertion(Comparator<E> comparator, Direction direction, Supplier<LinkedListCustom<E>> supplier) {
        LinkedListCustom<E> sortedCollection = getClone(supplier);
        Node<E> currentNode = sortedCollection.firstNode;
        for (int i = 2; i < sortedCollection.size + 1; i++) {
            currentNode = currentNode.next;
            Node<E> comparedNode = currentNode;
            for (int j = i; j > 1; j--) {
                if (direction.getDirection() * (comparator.compare(comparedNode.getContent(), comparedNode.previous.getContent())) < 0) {
                    Node<E> temp = new Node<>();
                    temp.setContent(comparedNode.previous.getContent());
                    comparedNode.previous.setContent(comparedNode.getContent());
                    comparedNode.setContent(temp.getContent());
                }
                comparedNode = comparedNode.previous;
            }
        }
        return sortedCollection;
    }

    public void printContent() {
        printNode(firstNode);
    }

    private void printNode(Node<E> node) {
        System.out.println(node);
        if (node.next != null) {
            printNode(node.next);
        }
    }

    @Override
    public String toString() {
        return "LinkedList{" + "firstNode=" + firstNode + ", lastNode=" + lastNode + ", size=" + size + '}';
    }
}
