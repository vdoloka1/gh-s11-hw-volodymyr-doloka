package org.geekhub.volodymyr.task2;

import java.util.ArrayList;
import java.util.List;

public class TaskList {
    private final List<Task> tasks = new ArrayList<>();

    public List<Task> getTasks() {
        return tasks;
    }

    public void add(Task task) {
        tasks.add(task);
    }

    @Override
    public String toString() {
        return "TaskManagerImpl{" +
                "tasks=" + tasks +
                '}';
    }
}
