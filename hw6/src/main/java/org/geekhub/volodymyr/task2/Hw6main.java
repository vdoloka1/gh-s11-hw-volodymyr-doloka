package org.geekhub.volodymyr.task2;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.HashSet;

public class Hw6main {

    public static void main(String[] args) {
        Task userTask1 = new Task(1, TaskType.IMPORTANT, "Do Homework", false,
                new HashSet<>(Arrays.asList("Study", "GeeKHub")), LocalDate.of(2021, 12, 15));
        Task userTask2 = new Task(2, TaskType.URGENT, "Do presentation for meeting", false,
                new HashSet<>(Arrays.asList("Work")), LocalDate.of(2021, 12, 25));
        Task userTask3 = new Task(3, TaskType.URGENT, "Buy Food", true,
                new HashSet<>(Arrays.asList("Home", "Food")), LocalDate.of(2021, 12, 24));
        Task userTask4 = new Task(4, TaskType.URGENT, "Do Homework", false,
                new HashSet<>(Arrays.asList("Study", "University")), LocalDate.of(2021, 12, 28));
        Task userTask5 = new Task(5, TaskType.IMPORTANT, "Call to friends", false,
                new HashSet<>(Arrays.asList("Home", "Friends")), LocalDate.of(2021, 12, 20));
        Task userTask6 = new Task(6, TaskType.IMPORTANT, "Celebrate NY", false,
                new HashSet<>(Arrays.asList("Home", "Сelebration", "Study")), LocalDate.of(2021, 12, 31));
        Task userTask7 = new Task(7, TaskType.REGULAR, "Buy vodka and cola", false,
                new HashSet<>(Arrays.asList("Home", "Сelebration")), LocalDate.of(2021, 12, 30));
        Task userTask8 = new Task(8, TaskType.IMPORTANT, "Buy vodka and pepsi", false,
                new HashSet<>(Arrays.asList("Home", "Сelebration", "Study")), LocalDate.of(2021, 9, 01));
        Task userTask9 = new Task(9, TaskType.IMPORTANT, "Buy mineral water", false,
                new HashSet<>(Arrays.asList("Home", "Сelebration")), LocalDate.of(2021, 01, 02));
        Task userTask10 = new Task(10, TaskType.IMPORTANT, "Buy mineral water and vodka and pepsi", false,
                new HashSet<>(Arrays.asList("Home", "Сelebration")), LocalDate.of(2022, 02, 01));
        TaskList sheduler = new TaskList();
        sheduler.add(userTask1);
        sheduler.add(userTask2);
        sheduler.add(userTask3);
        sheduler.add(userTask4);
        sheduler.add(userTask5);
        sheduler.add(userTask6);
        sheduler.add(userTask7);
        sheduler.add(userTask8);
        sheduler.add(userTask9);
        sheduler.add(userTask10);
        TaskManager shedulerManager = new TaskManagerImpl();
        System.out.println("\n" + "5 important undone task closest to current date:");
        System.out.println(shedulerManager.find5NearestImportantTasks(sheduler.getTasks()));
        System.out.println("\n" + "get list of all unique categories from all tasks");
        System.out.println(shedulerManager.getUniqueCategories(sheduler.getTasks()));
        System.out.println("\n" + "getCategoriesWithTasks");
        shedulerManager.getCategoriesWithTasks(sheduler.getTasks()).forEach((key, value) -> System.out.println(key + ":" + value));
        System.out.println("\n" + "splitTasksIntoDoneAndInProgress");
        shedulerManager.splitTasksIntoDoneAndInProgress(sheduler.getTasks()).forEach((key, value) -> System.out.println(key + ":" + value));
        System.out.println("\n" + "existsTaskOfCategory");
        System.out.println(shedulerManager.existsTaskOfCategory(sheduler.getTasks(), "Home"));
        System.out.println("\n" + "findTaskWithBiggestCountOfCategories");
        System.out.println(shedulerManager.findTaskWithBiggestCountOfCategories(sheduler.getTasks()));
        System.out.println("\n" + "getTitlesOfTasks");
        System.out.println(shedulerManager.getTitlesOfTasks(sheduler.getTasks(), 2, 4));
        System.out.println("\n" + "getCountsByCategories");
        System.out.println(shedulerManager.getCountsByCategories(sheduler.getTasks()));
        System.out.println("\n" + "getCategoriesNamesLengthStatistics");
        System.out.println(shedulerManager.getCategoriesNamesLengthStatistics(sheduler.getTasks()));
    }
}
