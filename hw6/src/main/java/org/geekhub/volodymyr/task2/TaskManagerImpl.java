package org.geekhub.volodymyr.task2;

import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

public class TaskManagerImpl implements TaskManager {

    @Override
    public List<Task> find5NearestImportantTasks(List<Task> tasks) {
        return tasks.stream()
                .filter((t) -> !t.isDone())
                .filter((t) -> t.getType().equals(TaskType.IMPORTANT))
                .sorted(Comparator.comparing(Task::getStartsOn))
                .filter((t) -> t.getStartsOn().isAfter(LocalDate.now()))
                .limit(5)
                .collect(Collectors.toList());
    }

    @Override
    public List<String> getUniqueCategories(List<Task> tasks) {
        return tasks.stream()
                .map(Task::getCategories)
                .flatMap(Collection::stream)
                .distinct()
                .sorted()
                .collect(Collectors.toList());
    }

    @Override
    public Map<String, List<Task>> getCategoriesWithTasks(List<Task> tasks) {
        return getUniqueCategories(tasks).stream()
                .collect(Collectors.toMap((c) -> c, (c) -> tasks.stream()
                        .filter((t) -> t.getCategories().contains(c))
                        .collect(Collectors.toList())));
    }

    @Override
    public Map<Boolean, List<Task>> splitTasksIntoDoneAndInProgress(List<Task> tasks) {
        return tasks.stream()
                .collect(Collectors.partitioningBy(Task::isDone));
    }

    @Override
    public boolean existsTaskOfCategory(List<Task> tasks, String category) {
        return getCategoriesWithTasks(tasks).get(category).size() > 0;
    }

    @Override
    public String getTitlesOfTasks(List<Task> tasks, int startNo, int endNo) {
        return tasks.stream()
                .skip(startNo - 1)
                .limit(endNo - startNo + 1)
                .map(Task::getTitle)
                .collect(Collectors.joining(","));
    }

    @Override
    public Map<String, Long> getCountsByCategories(List<Task> tasks) {
        return getUniqueCategories(tasks).stream()
                .collect(Collectors.toMap((c) -> c, (c) -> Long.valueOf(getCategoriesWithTasks(tasks).get(c).size())));
    }

    @Override
    public IntSummaryStatistics getCategoriesNamesLengthStatistics(List<Task> tasks) {
        return tasks.stream()
                .map(Task::getCategories)
                .flatMap(Collection::stream)
                .sorted()
                .distinct()
                .collect(Collectors.summarizingInt(String::length));
    }

    @Override
    public Task findTaskWithBiggestCountOfCategories(List<Task> tasks) {
        return tasks.stream()
                .max(Comparator.comparingInt(task -> task.getCategories().size()))
                .orElseThrow();
    }
}
