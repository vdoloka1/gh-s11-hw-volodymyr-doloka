package org.geekhub.volodymyr.task2;

import java.util.IntSummaryStatistics;
import java.util.List;
import java.util.Map;

public interface TaskManager {
    List<Task> find5NearestImportantTasks(List<Task> tasks);

    List<String> getUniqueCategories(List<Task> tasks);

    Map<String, List<Task>> getCategoriesWithTasks(List<Task> tasks);

    Map<Boolean, List<Task>> splitTasksIntoDoneAndInProgress(List<Task> tasks);

    boolean existsTaskOfCategory(List<Task> tasks, String category);

    String getTitlesOfTasks(List<Task> tasks, int startNo, int endNo);

    Map<String, Long> getCountsByCategories(List<Task> tasks);

    IntSummaryStatistics getCategoriesNamesLengthStatistics(List<Task> tasks);

    Task findTaskWithBiggestCountOfCategories(List<Task> tasks);
}
