package org.geekhub.volodymyr;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class Environment {

    private final Properties properties;

    public Environment(String propertyFileName) throws IOException {
        try {
            this.properties = new Properties();
            InputStream propertiesStream = Thread.currentThread().getContextClassLoader().getResourceAsStream(propertyFileName);
            properties.load(propertiesStream);
        } catch (Exception e) {
            throw (e);
        }
    }

    public String getProperty(String property) {
        return properties.getProperty(property);
    }
}
