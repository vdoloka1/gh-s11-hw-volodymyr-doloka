package org.geekhub.volodymyr.storage;

public class TableNameResolver {

    public <T extends Entity> String resolve(Class<T> clazz) {
        String tableName;
        if (clazz.isAnnotationPresent(Table.class)) {
            tableName = clazz.getAnnotation(Table.class).name();
        } else {
            tableName = clazz.getSimpleName().toLowerCase();
        }
        return tableName;
    }
}