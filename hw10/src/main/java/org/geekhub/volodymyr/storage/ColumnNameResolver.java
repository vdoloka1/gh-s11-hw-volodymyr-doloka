package org.geekhub.volodymyr.storage;

import java.lang.reflect.Field;

public class ColumnNameResolver {
    public <T extends Entity> String resolve(Field field) {
        String fieldName;
        if (field.isAnnotationPresent(Column.class)) {
            fieldName = field.getAnnotation(Column.class).name();
        } else {
            fieldName = field.getName().toLowerCase();
        }
        return fieldName;
    }

    public <T extends Entity> Field resolveField(String fieldName, Class<T> clazz) {
        Field[] fields = clazz.getDeclaredFields();
        Field matchedField = null;
        for (Field field : fields) {
            if (field.getName().equals(fieldName)) {
                matchedField = field;
            }
        }
        return matchedField;
    }
}
