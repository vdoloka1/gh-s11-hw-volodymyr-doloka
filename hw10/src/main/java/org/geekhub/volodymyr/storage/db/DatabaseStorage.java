package org.geekhub.volodymyr.storage.db;

import org.geekhub.volodymyr.storage.*;

import javax.sql.DataSource;
import java.lang.reflect.Field;
import java.sql.*;
import java.util.*;

public class DatabaseStorage implements Storage {
    private final DataSource dataSource;
    private final TableNameResolver tableNameResolver = new TableNameResolver();
    private final ColumnNameResolver columnNameResolver = new ColumnNameResolver();

    public DatabaseStorage(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public <T extends Entity> T get(Class<T> clazz, Integer id) throws StorageException {
        String sql = "SELECT * FROM \"" + tableNameResolver.resolve(clazz) + "\" WHERE id = " + id;
        try (Connection connection = dataSource.getConnection()) {
            Statement statement = connection.createStatement();
            List<T> result = extractResult(clazz, statement.executeQuery(sql));
            connection.close();
            return result.isEmpty() ? null : result.get(0);

        } catch (StorageException e) {
            throw e;
        } catch (Exception e) {
            throw new StorageException(e);
        }
    }

    @Override
    public <T extends Entity> List<T> list(Class<T> clazz) throws StorageException {
        String sql = "SELECT * FROM \"" + tableNameResolver.resolve(clazz) + "\"";
        try (Connection connection = dataSource.getConnection()) {
            Statement statement = connection.createStatement();
            return extractResult(clazz, statement.executeQuery(sql));
        } catch (StorageException e) {
            throw e;
        } catch (Exception e) {
            throw new StorageException(e);
        }
    }

    @Override
    public <T extends Entity> boolean delete(T entity) throws StorageException {
        Class<? extends Entity> clazz = entity.getClass();
        String sql = "DELETE FROM \"" + tableNameResolver.resolve(clazz) + "\" WHERE id = '" + entity.getId() + "'";
        try (Connection connection = dataSource.getConnection()) {
            Statement statement = connection.createStatement();
            int resultedCountDeletedRows = statement.executeUpdate(sql);

            return resultedCountDeletedRows > 0;
        } catch (Exception e) {
            throw new StorageException(e);
        }
    }

    @Override
    public <T extends Entity> void save(T entity) throws StorageException {
        if (entity.isNew()) {
            insert(entity);
        } else {
            update(entity);
        }
    }

    public <T extends Entity> void insert(T entity) throws StorageException {
        StringJoiner dbFields = new StringJoiner(",");
        StringJoiner dbValuePlaces = new StringJoiner(",");
        Map<String, Object> entityFieldsMap = prepareEntity(entity);
        for (Map.Entry<String, Object> entry : entityFieldsMap.entrySet()) {
            dbFields.add(entry.getKey());
            dbValuePlaces.add("?");
        }

        Class<? extends Entity> clazz = entity.getClass();
        String sql = "INSERT INTO \"" + tableNameResolver.resolve(clazz) + "\" (" + dbFields + ") VALUES(" + dbValuePlaces + ") RETURNING id ";
        int i = 1;

        try (Connection connection = dataSource.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(sql)
        ) {
            for (Map.Entry<String, Object> entry : entityFieldsMap.entrySet()) {
                preparedStatement.setObject(i, entry.getValue());
                i++;
            }
            try (ResultSet rs = preparedStatement.executeQuery()) {
                if (rs.next()) {
                    entity.setId(rs.getInt(1));
                }
            }
        } catch (Exception e) {
            throw new StorageException(e);
        }
    }

    public <T extends Entity> void update(T entity) throws StorageException {
        StringJoiner dbFields = new StringJoiner(",");
        StringJoiner dbFieldsAndValues = new StringJoiner(", ");
        Map<String, Object> entityFieldsMap = prepareEntity(entity);
        for (Map.Entry<String, Object> mapEntry : entityFieldsMap.entrySet()) {
            String key = mapEntry.getKey();
            dbFields.add(key);
            dbFieldsAndValues.add(key + " = ?");
        }
        Class<? extends Entity> clazz = entity.getClass();
        String sql = "UPDATE \"" + tableNameResolver.resolve(clazz) + "\" SET " + dbFieldsAndValues
                + " WHERE id = " + entity.getId();
        int i = 1;

        try (Connection connection = dataSource.getConnection(); PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            for (Map.Entry<String, Object> entry : entityFieldsMap.entrySet()) {
                preparedStatement.setObject(i, entry.getValue());
                i++;
            }
            preparedStatement.executeUpdate();
        } catch (Exception e) {
            throw new StorageException(e);
        }
    }

    private <T extends Entity> List<T> extractResult(Class<T> clazz, ResultSet resultSet) throws Exception {
        List<T> resultList = new ArrayList<>();
        while (resultSet.next()) {
            T newClazzInstance = clazz.getDeclaredConstructor().newInstance();
            Class<?> extractedClazz = clazz;
            while (extractedClazz != null) {
                Field[] fieldsSuperClass = extractedClazz.getDeclaredFields();
                for (Field field : fieldsSuperClass) {
                    if (!field.isAnnotationPresent(Ignore.class)) {
                        field.setAccessible(true);
                        Object fieldFromDB = resultSet.getObject(columnNameResolver.resolve(field));
                        try {
                            field.set(newClazzInstance, fieldFromDB);
                        } catch (Exception e) {
                            throw new StorageException(e);
                        }
                    }
                }
                extractedClazz = extractedClazz.getSuperclass();
            }
            resultList.add(newClazzInstance);
        }

        return resultList;
    }

    private <T extends Entity> Map<String, Object> prepareEntity(T entity) throws StorageException {
        Class<? extends Entity> clazz = entity.getClass();
        Field[] fields = clazz.getDeclaredFields();
        Map<String, Object> entityFieldsMap = new HashMap<>();
        for (Field field : fields) {
            if (!field.isAnnotationPresent(Ignore.class)) {
                field.setAccessible(true);
                try {
                    entityFieldsMap.put(columnNameResolver.resolve(field), field.get(entity));
                } catch (Exception e) {
                    throw new StorageException(e);
                }
            }
        }

        return entityFieldsMap;
    }
}