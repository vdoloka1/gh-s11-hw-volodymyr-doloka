package org.geekhub.volodymyr.storage;

public class StorageException extends Exception {

    public StorageException(Throwable cause) {
        super(cause);
    }
}