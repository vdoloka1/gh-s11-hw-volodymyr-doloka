CREATE TABLE IF NOT EXISTS "cat"
(
    "id"   SERIAL      NOT NULL,
    "name" VARCHAR(20) NOT NULL,
    "age"  INTEGER     NOT NULL,
    PRIMARY KEY ("id")
);

CREATE TABLE IF NOT EXISTS "user"
(
    "id"      SERIAL           NOT NULL,
    "name"    VARCHAR(30)      NOT NULL,
    "age"     INTEGER          NOT NULL,
    "admin" BOOLEAN          NOT NULL,
    "balance" DOUBLE PRECISION NOT NULL,
    PRIMARY KEY ("id")
);