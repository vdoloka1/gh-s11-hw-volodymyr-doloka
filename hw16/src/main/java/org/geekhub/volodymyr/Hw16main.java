package org.geekhub.volodymyr;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Hw16main {
    public static void main(String[] args) {
        SpringApplication.run(Hw16main.class, args);
    }
}
