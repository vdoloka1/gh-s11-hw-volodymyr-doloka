package org.geekhub.volodymyr.repository.mapper;

import org.geekhub.volodymyr.repository.GuestBookEntry;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class GuestBookRowMapper implements RowMapper {
    @Override
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        GuestBookEntry guestBookEntry = new GuestBookEntry(rs.getString("name"),
                rs.getString("message"),
                rs.getInt("rating"),
                rs.getTimestamp("date").toLocalDateTime());
        return guestBookEntry;
    }
}
