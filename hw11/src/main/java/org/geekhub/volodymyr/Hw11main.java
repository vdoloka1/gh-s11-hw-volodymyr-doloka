package org.geekhub.volodymyr;

import java.io.IOException;


public class Hw11main {

    public static void main(String[] args) {
        String inputFilePath = "hw11\\src\\main\\resources\\input.txt";
        String outputFilePath = "hw11\\src\\main\\resources\\output.txt";
        try {
            WebUtils webUtils = new WebUtils(inputFilePath, outputFilePath);
            webUtils.SaveUrlContentMD5();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
