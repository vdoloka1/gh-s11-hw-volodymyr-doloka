package org.geekhub.volodymyr;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static java.nio.file.Files.lines;

public class WebUtils {
    private final String inputFile;
    private final String outputFile;

    public String getMD5ofUrlContent(String url) throws IOException, NoSuchAlgorithmException {
        InputStream inputStream = new URL(url).openStream();
        String urlContent = new String(inputStream.readAllBytes());
        String MD5 = md5Calculate(urlContent);
        System.out.println(MD5);
        return MD5;
    }

    private String md5Calculate(String content) throws NoSuchAlgorithmException {
        MessageDigest messageDigest;
        byte[] digest;
        messageDigest = MessageDigest.getInstance("MD5");
        messageDigest.reset();
        messageDigest.update(content.getBytes());
        digest = messageDigest.digest();
        BigInteger bigInt = new BigInteger(1, digest);
        StringBuilder md5Hex = new StringBuilder(bigInt.toString(16));
        while (md5Hex.length() < 32) {
            md5Hex.insert(0, "0");
        }
        md5Hex.append("\n");
        return md5Hex.toString();
    }

    public void SaveUrlContentMD5() throws IOException {
        Path inputPath = Paths.get(inputFile);
        ExecutorService executorWebService = Executors.newCachedThreadPool();
        lines(inputPath).forEach(url -> executorWebService.submit(() -> {
            System.out.println(Thread.currentThread().getName());
            String md5String = null;
            try {
                md5String = getMD5ofUrlContent(url);
            } catch (IOException | NoSuchAlgorithmException e) {
                e.printStackTrace();
            }
            try {
                saveToFile(url, md5String);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }));
        executorWebService.shutdownNow();
    }

    private synchronized void saveToFile(String url, String md5) throws Exception {
        Path outputPath = Paths.get(outputFile);
        String outputString = url + " " + md5;
        byte[] bytes = outputString.getBytes();
        Files.write(outputPath, bytes, StandardOpenOption.APPEND);
    }

    public WebUtils(String inputFile, String outputFile) throws IOException {
        this.inputFile = inputFile;
        this.outputFile = outputFile;
        Path outputPath = Paths.get(outputFile);
        Files.deleteIfExists(outputPath);
        Files.createFile(outputPath);
    }
}