package org.geekhub.volodymyr;

import java.io.IOException;

public class Hw12main {

    public static void main(String[] args) {
        final String urlTemplate = "https://www.gettyimages.com/photos/travel?assettype=image&license=" +
                "rf&alloweduse=availableforalluses&embeddable=true&family=creative&phrase=travel&sort=best&page=";
        final int imgQuantity = 200;
        final String outputDir = "hw12\\src\\main\\resources\\output\\";
        try {
            ImageCrawler imgCrawler = new ImageCrawler(urlTemplate, imgQuantity, outputDir);
            imgCrawler.saveImages();
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }
    }
}
