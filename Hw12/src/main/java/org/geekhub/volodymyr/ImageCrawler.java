package org.geekhub.volodymyr;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;

public class ImageCrawler {
    final ExecutorService executorWebService;
    final AtomicInteger fileCounter;
    final String outputDir;
    final String urlTemplate;
    final int imgQuantity;

    public ImageCrawler(String urlTemplate, int imageQuantity, String outputDir) {
        this.urlTemplate = urlTemplate;
        this.imgQuantity = imageQuantity;
        this.outputDir = outputDir;
        executorWebService = Executors.newCachedThreadPool();
        fileCounter = new AtomicInteger();
    }

    public void createOutputDir() throws IOException {
        Path outputDirPath = Paths.get(outputDir);
        Files.createDirectories(outputDirPath);
    }

    public void saveImages() throws IOException {
        createOutputDir();
        int parseCounter = 0;
        int currentPage = 1;
        String urlPage = urlTemplate + currentPage;

        while (parseCounter < imgQuantity) {
            Document urlHTML = Jsoup.connect(urlPage).get();
            Elements elements = urlHTML.getElementsByTag("img");

            for (Element element : elements) {
                String imgUrl = element.attr("src");
                if (imgUrl.startsWith("https")) {
                    parseCounter++;
                    executorWebService.submit(() -> {
                        try {
                            saveImage(getImage(imgUrl));
                        } catch (IOException e) {
                            System.err.println(e.getMessage());
                        }
                    });
                    if (parseCounter == imgQuantity) {
                        break;
                    }
                }
            }
            urlPage = urlTemplate + ++currentPage;
        }
        executorWebService.shutdown();
    }


    private byte[] getImage(String url) throws IOException {

        return Jsoup.connect(url)
                .header("Accept-Encoding", "gzip, deflate")
                .userAgent("Mozilla/5.0 (Windows NT 6.1; WOW64; rv:23.0) Gecko/20100101 Firefox/23.0")
                .ignoreContentType(true)
                .maxBodySize(0)
                .timeout(60000)
                .execute()
                .bodyAsBytes();
    }

    void saveImage(byte[] bytes) {
        String outputFile = outputDir + "img" + fileCounter.incrementAndGet() + ".jpg";
        Path outputPath = Paths.get(outputFile);
        try {
            Files.write(outputPath, bytes, StandardOpenOption.CREATE);
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }
    }
}